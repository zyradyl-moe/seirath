---
title: Starstuck Quirk Generator
tags: ["javascript", "typescript", "hobby"]
---

The [Starstuck Quirk Generator][1] is a hobby project that I started to
simplify the process of allowing roleplaying partners to accurately and quickly
generate the typing quirks of their characters in a [Homestuck][2] based
roleplay group.

A fork of the [Troll Quirk Generator][3] by CommanderMark, it started as around
2200 lines of [Typescript][4], CSS, and HTML. My fork currently sits at about
7200 lines of code, most of it Typescript, and is built using [node.js][5].

The code primarily focuses on string editing and basic text replacing. It was
also my first ever Typescript project. The website itself is hosted on
[GitLab Pages][6] and is licensed under the [zlib license][7].

**Project Name:** Starstuck Quirk Generator v2.0 <br />
**Project URL:** https://starstuck.zyradyl.moe <br />
**Project License:** [zlib License][7] <br />
**Project Language:** [Typescript][4] <br />
**Forked From:** [Troll Quirk Generator][3] <br />
**Total Lines of Code Added:** ~5000

[1]: https://starstuck.zyradyl.moe
[2]: https://homestuck.com
[3]: https://github.com/CommanderMark/Troll-Quirk-Generator
[4]: https://www.typescriptlang.org/
[5]: https://nodejs.org/en/
[6]: https://docs.gitlab.com/ee/user/project/pages/
[7]: https://opensource.org/licenses/Zlib
